package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class PostServiceImpl implements PostService{

    @Autowired
    private PostRepository postRepository;
    @Autowired

    private UserRepository userRepository;
    @Autowired

    JwtToken jwtToken;

    public void createPost(String stringToken, Post post){
        //findByUsername to retrieve the user
        //Criteria for finding the user is from the jwtToken method(getUsernameFromToken)
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Post newPost = new Post();
        //Title and content will come from the reqBody which is pass through the "post" identifier
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        //author retrieved from the token
        newPost.setUser(author);
        //actual saving of post
        postRepository.save(newPost);

    }

    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }


    //Edit a post
    public ResponseEntity updatePost(Long id, String stringToken, Post post){

        Post postForUpdating = postRepository.findById(id).get();   //we will retrieve the post for updating using the "id" provided

        String postAuthor = postForUpdating.getUser().getUsername();    //Because relationship is established within the User and Post model, we are able to retrieve the username of the post owner

        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(postAuthor)){
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());

            postRepository.save(postForUpdating);

            return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);

        }else {
            return new ResponseEntity<>("You are not authorized to edit this post", HttpStatus.UNAUTHORIZED);
        }


    }

    //delete post
    public ResponseEntity deletePost(Long id, String stringToken){
        Post postForUpdating = postRepository.findById(id).get();

        String postAuthor = postForUpdating.getUser().getUsername();

        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(postAuthor)){
            postRepository.deleteById(id);

            return new ResponseEntity<>("Post deleted successfully", HttpStatus.OK);
        }else {
            return new ResponseEntity<>("You are not Authorized to edit this post", HttpStatus.UNAUTHORIZED);
        }
    }

    public Set<Post> getUserPosts(String stringToken) {
        String Author = jwtToken.getUsernameFromToken(stringToken);

        User user = userRepository.findByUsername(Author);



        return user.getPosts();
    }



}
