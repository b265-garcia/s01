package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

import java.util.Set;

public interface PostService {

    //create a post
    void createPost(String stringToken, Post post);

    //getting all posts
    Iterable<Post> getPosts();
    //edit a post
    ResponseEntity updatePost(Long id, String stringToken, Post post);
    //delete a post
    ResponseEntity deletePost(Long id, String stringToken);

    Set<Post> getUserPosts(String stringToken);

}
