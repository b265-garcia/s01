package com.zuitt.wdc044.models;

import javax.persistence.*;

//mark this Java Object as a representation of a database table via @Entity
@Entity

@Table(name="posts")    //designate table name via @Table
public class Post {

    @Id     //indicate that hthis property represents the primary key via @Id
    @GeneratedValue     //values for this property will be auto-incremented
    private Long id;

    @Column
    private String title;
    @Column
    private String content;


    @ManyToOne      //represent many side of the relationship
    @JoinColumn(name ="user_id", nullable = false)  //to reference the foreign key column
    private User user;

    public Post(){}

    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    public String getTitle(){
        return title;
    }

    public String getContent(){
        return content;
    }
    public void setTitle(String title){
        this.title = title;
    }
    public void setContent(String content){
        this.content = content;
    }

    public User getUser(){
        return user;
    }
    public void setUser(User user){
        this.user = user;
    }


}
